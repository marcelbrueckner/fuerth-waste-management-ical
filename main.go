package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/arran4/golang-ical"
)

func main() {
	//
	// Verify waste schedule ID is set
	//
	id, isSet := os.LookupEnv("FUERTH_WASTE_SCHEDULE_ID")
	if !isSet {
		fmt.Println("Expected FUERTH_WASTE_SCHEDULE_ID to be set.")
		os.Exit(1)
	}

	//
	// Download and parse calendar
	//
	url := fmt.Sprintf("https://abfallwirtschaft.fuerth.eu/termine.php?icalexport=%s", id)
	content := getContentFromUrl(url)
	if content == "" {
		fmt.Println("Received empty calendar file.")
		os.Exit(1)
	}

	cal, err := ics.ParseCalendar(strings.NewReader(content))
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	//
	// Adjust calendar entries
	//
	dtstamp := time.Date(time.Now().Year(), 1, 1, 0, 0, 0, 0, time.UTC)

	// X-WR-CALNAME contains the address for which the calendar has been generated
	// Remove for privacy reasons as the final calendar file will be publicly available
	cal.SetXWRCalName("Abfuhrtermine")

	for _, event := range cal.Events() {
		event.SetDtStampTime(dtstamp)
		// Description contains the address for which the calendar has been generated
		// As the SUMMARY says it all, this property can be removed
		// (unfortunately, the library doesn't provide a convenient function for that)
		for i, property := range event.Properties {
			if property.IANAToken == string(ics.ComponentPropertyDescription) {
				event.Properties = append(event.Properties[:i], event.Properties[i+1:]...)
				break
			}
		}

		summary := event.GetProperty(ics.ComponentPropertySummary).Value
		dtstart := event.GetProperty(ics.ComponentPropertyDtStart).Value
		uid := fmt.Sprintf("%s-%s@abfallwirtschaft.fuerth.eu", dtstart, strings.ReplaceAll(summary, " ", ""))
		event.SetProperty(ics.ComponentPropertyUniqueId, uid)
	}

	//
	// Write serialized calendar to file
	//
	filename, isSet := os.LookupEnv("FUERTH_WASTE_FILENAME")
	if !isSet {
		filename = "fuerth-waste-schedule.ics"
	}
	serialized := cal.Serialize()
	serializedBytes := []byte(serialized)
    err = ioutil.WriteFile(filename, serializedBytes, 0644)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

// Download calendar file and read content into string
// https://stackoverflow.com/questions/38673673/access-http-response-as-string-in-go
func getContentFromUrl(url string) (string) {
	response, err := http.Get(url)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	
	defer response.Body.Close()

	if response.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(response.Body)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		return string(bodyBytes)
	}

	return ""
}
