# Fürth waste management iCal

Grabs an iCalendar file from [Fürth's waste management site](https://abfallwirtschaft.fuerth.eu/) and publishes it via GitLab Pages.

## Why?

Fürth's waste management offers a waste collection schedule as iCalendar file export. While the schedule as such rarely changes throughout the year, the file itself is generated on each request, leading to slightly longer request times on the one hand. On the other hand, it also constantly generates new event UIDs that every client handles differently. For example, iOS' native calendar application wipes the local calendar cache and re-downloading it on every refresh. Other clients, like this [custom iCalendar component for Home Assistant](https://github.com/KoljaWindeler/ics) rejects the regular iCalendar export URL (which resembles to `https://abfallwirtschaft.fuerth.eu/termine.php?icalexport=CUSTOM_SCHEDULE_ID`) at all.

## How does it work?

1. Download iCalendar file
2. Remove personal information (address the schedule is generated for), generation timestamps and everything else that's not needed for the calendar to work (see [RFC 5545, Section 3.6](https://datatracker.ietf.org/doc/html/rfc5545#section-3.6)). Thus, the published file will only be changed when there are actual changes to the schedules.
3. Publish via GitLab Pages

### Example

From this

```ical
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//abfallwirtschaft.fuerth.eu//NONSGML kigkonsult.se iCalcreator 2.2
 0.2//
METHOD:PUBLISH
X-WR-CALNAME:Abfuhrtermine für: REDACTED
X-WR-CALDESC:Abfallwirtschaft der Stadt Fürth
X-WR-TIMEZONE:Europe/Berlin
BEGIN:VEVENT
UID:20210601T001854CEST-5277prRJDp@abfallwirtschaft.fuerth.eu
DTSTAMP:20210531T221854Z
DESCRIPTION:Abfuhrtermin Restabfall REDACTED
DTSTART;VALUE=DATE:20210104
DTEND;VALUE=DATE:20210105
SUMMARY:Restabfall
END:VEVENT
BEGIN:VEVENT
UID:20210601T001854CEST-5280Ir0iv6@abfallwirtschaft.fuerth.eu
DTSTAMP:20210531T221854Z
DESCRIPTION:Abfuhrtermin Restabfall REDACTED
DTSTART;VALUE=DATE:20210118
DTEND;VALUE=DATE:20210119
SUMMARY:Restabfall
END:VEVENT
```

to this

```ical
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//abfallwirtschaft.fuerth.eu//NONSGML kigkonsult.se iCalcreator 2.2
 0.2//
METHOD:PUBLISH
X-WR-CALNAME:Abfuhrtermine
X-WR-CALDESC:Abfallwirtschaft der Stadt Fürth
X-WR-TIMEZONE:Europe/Berlin
BEGIN:VEVENT
UID:20210104-Restabfall@abfallwirtschaft.fuerth.eu
DTSTAMP:20210101T000000Z
DTSTART;VALUE=DATE:20210104
DTEND;VALUE=DATE:20210105
SUMMARY:Restabfall
END:VEVENT
BEGIN:VEVENT
UID:20210118-Restabfall@abfallwirtschaft.fuerth.eu
DTSTAMP:20210101T000000Z
DTSTART;VALUE=DATE:20210118
DTEND;VALUE=DATE:20210119
SUMMARY:Restabfall
END:VEVENT
```

## License

MIT
